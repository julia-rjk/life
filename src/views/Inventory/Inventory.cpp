#include "Inventory.hpp"

Inventory::Inventory(){
	cadre.setSize(sf::Vector2f(865, 400));
	cadre.setOutlineColor(sf::Color::Black);
	cadre.setOutlineThickness(5);
	cadre.setPosition(-8000, -4000);
	texture.loadFromFile("content/inventory.png");
	cadre.setTexture(&texture);
	show = false;


	// Add coins sprite
	textureCoin.loadFromFile("content/Coin/goldCoin1.png");
	spriteCoin.setTexture(textureCoin);
	spriteCoin.setPosition(-500,-500);
	spriteCoin.setScale(2,2);
	allSprites.push_back(spriteCoin);

	sf::Text textCoin;
	textCoin.setString( "0 x ");
	allText.push_back(textCoin);

}

void Inventory::setImagePlayer(std::string path){
	texturePlayer.loadFromFile(path);

	// Add Player's sprite
	spritePlayer.setTexture(texturePlayer);
	spritePlayer.setScale(6,6);
	spritePlayer.setPosition(-500,-500);
	allSprites.push_back(spritePlayer);

}

bool Inventory::getShow(){
	return show;
}

void Inventory::setCoin(int coin){
	sf::Text textCoin;
	textCoin.setString( coin + " x");
	allText.push_back(textCoin);
}

void Inventory::setShow(bool cond){
	this->show = cond;
}

void Inventory::showInventory(){
	if(show){
		cadre.setPosition(20, 100);
		allSprites[1].setPosition(90,180);
		allSprites[0].setPosition(110,400);
		allText[0].setPosition(110,500);
		show=false;
	}else{
		cadre.setPosition(-8000, -4000);
		allSprites[0].setPosition(-20,-250);
		allSprites[1].setPosition(-500,-500);
		allText[0].setPosition(-500,-500);
		show = true;
	}
}

sf::RectangleShape Inventory::getCadre(){
	return cadre;
}

std::vector<sf::Sprite> Inventory::getAllSprites(){
	return allSprites;
}

std::vector<sf::Text> Inventory::getAllText(){
	return allText;
}

sf::Text Inventory::getText(int i){
	return allText[i];
}

sf::Sprite Inventory::getSprite(int i){
	return allSprites[i];
}
