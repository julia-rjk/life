#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

class Inventory {
	public:
	bool show;
	sf::Texture texture, texturePlayer, textureCoin;
	sf::Sprite spritePlayer;
	sf::Sprite spriteCoin;

	sf::RectangleShape cadre;
	std::vector<sf::Sprite> allSprites;
	std::vector<sf::Text> allText;
	std::vector<sf::Text> getAllText();
	sf::Text getText(int i);

	void showInventory();
	Inventory();
	void setImagePlayer(std::string path);
	sf::RectangleShape getCadre();
	bool getShow();
	void setShow(bool cond);
	std::vector<sf::Sprite> getAllSprites();
	sf::Sprite getSprite(int i);
	void setCoin(int coin);
};


#endif //INVENTORY_HPP