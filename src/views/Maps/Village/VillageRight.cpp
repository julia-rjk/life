#include "VillageRight.hpp"

VillageRight::VillageRight(Player player){

	sf::RenderWindow window;
	window.create(sf::VideoMode(900, 600),"Life");
	// sf::Vector2u windowSize = window.getSize();

	sf::Clock clock;
	Inventory inventory;
	player.setPosition(450,50);
	inventory.setImagePlayer("content/Character/down.png");

	initCollision();

	//Variable qui permet de gérer pour qu'une seule fenêtre soit ouverte
	// bool somethingIsOpen = false;

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/Map2.png");
	shape.setTexture(&shapeTexture);


	//Charge l'ecriture de début
	sf::Font font;
	font.loadFromFile("font/pixel.TTF");
	sf::Music soundWalk;
	if (!soundWalk.openFromFile("content/sound/walk_stone.ogg"))
		std::cout<<"Erreur sur le chargement de la musique";
	sf::Text textInventory;
	textInventory.setString("Inventory");
	textInventory.setFont(font);
	textInventory.setCharacterSize(30);
	textInventory.setPosition(-100,-100);


	while(window.isOpen()){
		sf::Event event;

		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				if(player.getClockAnimation().getElapsedTime().asSeconds() > 0.25){
					player.getClockAnimation().restart();
				}
				if(canGo(player,3)){
					player.moveLeft();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}else{
					player.moveRight();
				}

			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				if(canGo(player,2)){
					player.moveRight();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
				else{
						player.moveLeft();
					}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				if(canGo(player,0)){
					player.moveUp();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}

				}
				else
					{
						player.moveDown();

					}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				if(canGo(player,1)){
					player.moveDown();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
					}
				else{
					player.moveUp();
				}
			}
		}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)){
					if(clock.getElapsedTime().asSeconds() > 0.2f){
						inventory.showInventory();
						textInventory.setPosition(inventory.getCadre().getPosition().x +350, inventory.getCadre().getPosition().y+20);
						clock.restart();
					}
				}


		std::cout<< " x : "<< player.getPlayer().getPosition().x <<endl;
		std::cout<< " y : "<< player.getPlayer().getPosition().y <<endl;

		if(player.getPlayer().getPosition().y < 0){
			window.close();
			StartGame start(player);
		}





		window.clear(sf::Color::Black);
	//
		window.draw(shape);
		window.draw(player.getPlayer());
		window.draw(inventory.getCadre());
		window.draw(textInventory);
		for(std::size_t i=0; i<inventory.getAllSprites().size(); i++){
			window.draw(inventory.getSprite(i));
		}
	//
		window.display();
	}

}

void VillageRight::initCollision(){
	sf::RectangleShape collisionBox;

	collisionBox.setSize(sf::Vector2f(900-480,195));
	collisionBox.setPosition(495,0);
	collision.push_back(collisionBox);

	collisionBox.setSize(sf::Vector2f(390,600-270));
	collisionBox.setPosition(0,270);
	collision.push_back(collisionBox);
}

bool VillageRight::canGo(Player player, int x){
	// 0 for up
	// 1 for down
	// 2 for right
	// 3 for left
	for(std::size_t i=0; i<collision.size(); i++){
		switch(x){
			case 0:
				player.moveUp();
				break;
			case 1:
				player.moveDown();
				break;
			case 2:
				player.moveRight();
				break;
			case 3:
				player.moveLeft();
				break;
			default:
				break;
		}


		if(player.getPlayer().getGlobalBounds().intersects(collision[i].getGlobalBounds()))
					return false;
		}

	return true;
}