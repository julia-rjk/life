#include "Village.hpp"

Village::Village(Player player){

	window.create(sf::VideoMode(900, 600),"Life");
	// sf::Vector2u windowSize = window.getSize();
	player.setPosition(450,50);
	inventory.setImagePlayer("content/Character/down.png");

	initCollision();

	//Variable qui permet de gérer pour qu'une seule fenêtre soit ouverte
	// bool somethingIsOpen = false;

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/Map2.png");
	shape.setTexture(&shapeTexture);

	// NPC

	textureNpc.loadFromFile("content/Character/npc_villager.png");
	spriteNpc.setTexture(textureNpc);
	spriteNpc.setPosition(600,200);
	spriteNpc.setScale(0.7,0.7);

	// Animation
	rectSourceSprite.height = 64;
	rectSourceSprite.width = 64;
	rectSourceSprite.top = 0;
	rectSourceSprite.left = 0;

	textureExclamation.loadFromFile("content/Animation/exclamation.png");

	spriteAnimation.setTexture(textureExclamation);
	spriteAnimation.setTextureRect(rectSourceSprite);
	spriteAnimation.setPosition(600,160);
	spriteAnimation.setScale(0.5,0.5);

	//Charge l'ecriture de début
	sf::Font font;
	font.loadFromFile("font/pixel.TTF");

	// Sound
	if (!soundWalk.openFromFile("content/sound/walk_stone.ogg")|| !soundExpression.openFromFile("content/sound/soundExpression.wav"))
		std::cout<<"Erreur sur le chargement de la musique";

	// Inventory
	sf::Text textInventory;
	textInventory.setString("Inventory");
	textInventory.setFont(font);
	textInventory.setCharacterSize(30);
	textInventory.setPosition(-100,-100);


	while(window.isOpen()){
		// Event
		sf::Event event;
		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				if(player.getClockAnimation().getElapsedTime().asSeconds() > 0.25){
					player.getClockAnimation().restart();
				}

				if(canGo(player,3)){
					player.moveLeft();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}else{ player.moveRight(); }
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
				sf::Texture textureSprite;

				if(canGo(player,2)){
					player.moveRight();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
				else{player.moveLeft();}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				if(canGo(player,0)){
					player.moveUp();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}

				}
				else
					{
						player.moveDown();

					}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				if(canGo(player,1)){
					player.moveDown();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
					}
				else{
					player.moveUp();
				}
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)){
			if(clock.getElapsedTime().asSeconds() > 0.2f){
				inventory.setCoin(player.getGold());
				inventory.showInventory();
				textInventory.setPosition(inventory.getCadre().getPosition().x +350, inventory.getCadre().getPosition().y+20);
				clock.restart();
			}
		}

		if(player.getPlayer().getPosition().y < 0){
			window.close();
			StartGame start(player);
		}

		if(player.getPlayer().getPosition().x > 900){
			window.close();
			VillageRight village(player);
		}
		animateNpc(player.getPlayer().getPosition().x, player.getPlayer().getPosition().y );

		window.clear(sf::Color::Black);
	//
		window.draw(shape);
		window.draw(player.getPlayer());
		window.draw(spriteNpc);
		window.draw(inventory.getCadre());
		window.draw(spriteAnimation);
		window.draw(textInventory);
		for(std::size_t i=0; i<inventory.getAllSprites().size(); i++){
			window.draw(inventory.getSprite(i));
		}
		for(std::size_t i=0; i<inventory.getAllText().size(); i++){
			window.draw(inventory.getText(i));
		}

	//
		window.display();
	}

}
void Village::animateNpc(int x, int y){
		if(clock.getElapsedTime().asSeconds()==2){
			soundExpression.play();
		}
			if (clockAnimation.getElapsedTime().asSeconds() > 0.01f){
				if(rectSourceSprite.top >= 256){
					spriteAnimation.setPosition(-3000,-3000);
					if(spriteNpc.getPosition().x > x) spriteNpc.move(-1,0);
					else{
						if(spriteNpc.getPosition().y > y-10) spriteNpc.move(0,-1);
					}
				}else{
					if (rectSourceSprite.left == 256){
						rectSourceSprite.left = 0;
						if(rectSourceSprite.top < 256) rectSourceSprite.top +=64;
					}
					else rectSourceSprite.left += 64;
					spriteAnimation.setTextureRect(rectSourceSprite);

				}
				clockAnimation.restart();
    		}
}
// Function to set box collision so the player can't go through
void Village::initCollision(){
	sf::RectangleShape collisionBox;

	collisionBox.setSize(sf::Vector2f(900-480,195));
	collisionBox.setPosition(495,0);
	collision.push_back(collisionBox);

	collisionBox.setSize(sf::Vector2f(390,600-270));
	collisionBox.setPosition(0,270);
	collision.push_back(collisionBox);
}

bool Village::canGo(Player player, int x){
	// 0 for up
	// 1 for down
	// 2 for right
	// 3 for left
	for(std::size_t i=0; i<collision.size(); i++){
		switch(x){
			case 0:
				player.moveUp();
				break;
			case 1:
				player.moveDown();
				break;
			case 2:
				player.moveRight();
				break;
			case 3:
				player.moveLeft();
				break;
			default:
				break;
		}


		if(player.getPlayer().getGlobalBounds().intersects(collision[i].getGlobalBounds()))
					return false;
		}

	return true;
}