#pragma once

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
#include "views/Inventory/Inventory.hpp"
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

class Player {
	public:
	sf::Texture textureSprite;
	sf::Sprite player;
	sf::Clock clockAnimation;
	Inventory inventory;
	int start;
	Player();
	void moveRight();
	void moveLeft();
	void moveDown();
	void moveUp();
	sf::Sprite getPlayer();
	sf::Clock getClockAnimation();
	void setPosition(int x,int y);
	Inventory getInventory();
	string getName();
	void setName(string newName);
	void setGold(int addGold);
	int getGold();
	void setRight(bool condition);
	bool getRight();
	void setTexture(std::string path);
	int getY();
	int getX();

	private:
		string name;
		int gold;
		bool right;

};
