#include "Menu.hpp"

int createMenu(){
	sf::RenderWindow window;
	window.create(sf::VideoMode(900, 600),"Life");
	sf::Vector2u windowSize = window.getSize();
	int width = sf::VideoMode::getDesktopMode().width;
	int height = sf::VideoMode::getDesktopMode().height;
	window.setPosition(sf::Vector2i(width/4, height/4));
	int selected = 1;

	sf::Texture texture,textureCursor;
	if(!texture.loadFromFile("content/UI/buttonRed.png")||!textureCursor.loadFromFile("content/UI/cursor.png")){
		return 1;
	}
	sf::Sprite sprite(texture);
	sf::Sprite spriteCursor(textureCursor);
	sf::Sprite button3(texture);
	sprite.setScale(0.2,0.2);
	button3.setScale(0.2,0.2);

	sprite.setPosition(sf::Vector2f ((windowSize.x/2)-80,windowSize.y/2));
	button3.setPosition(sf::Vector2f ((windowSize.x/2)-80,windowSize.y/2+150));

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/Ocean.png");
	shape.setTexture(&shapeTexture);

	sf::Music music,musicBack;
	if (!music.openFromFile("content/sound/click4.ogg")||!musicBack.openFromFile("content/sound/TownTheme.ogg"))
		std::cout<<"Erreur sur le chargement de la musique";

	//Charge l'ecriture de début
	sf::Font font;
	if (!font.loadFromFile("font/pixel.TTF")){
		return EXIT_FAILURE;
	}
	musicBack.play();
	musicBack.setVolume(5);
	musicBack.setLoop(true);

	//Création Label
	sf::Text text("LIFE", font, 50);
	text.setPosition(385,50);

	sf::Text textButton1("Play", font, 24);
	textButton1.setPosition(sprite.getPosition().x+60,sprite.getPosition().y+30);
	sf::Text textButton3("Quit", font, 24);
	textButton3.setPosition(button3.getPosition().x+60,button3.getPosition().y+30);

	//Mise en place de la souris
	sf::Vector2f mp;
	mp.x = sf::Mouse::getPosition(window).x;
	mp.y = sf::Mouse::getPosition(window).y;

	while(window.isOpen()){
		sf::Event event;

		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
				music.play();
				window.close();
				switch(selected){
					case 1:{
						musicBack.stop();
						MenuName menuName;
						break;
					}
					case 2:
					break;
					case 3:
					window.close();
					break;
					default:
					break;
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				music.play();
				if (selected >1)
					--selected;
				else selected=2;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				music.play();
				if (selected <2)
					++selected;
				else selected =1;
			}
		}

		switch(selected){
			case 1:
			spriteCursor.setPosition(sprite.getPosition().x-50, sprite.getPosition().y+20);
			break;
			case 2:
			spriteCursor.setPosition(button3.getPosition().x-50, button3.getPosition().y+20);
			break;
			default:
			break;
		}


		window.clear(sf::Color::Black);
	//
		window.draw(shape);
		window.draw(sprite);
		window.draw(button3);
		window.draw(text);
		window.draw(textButton1);
		window.draw(textButton3);
		window.draw(spriteCursor);

	//
		window.display();

	}
	return 0;
}