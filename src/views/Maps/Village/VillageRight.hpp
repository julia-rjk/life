#pragma once

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
#include "views/Player/player.hpp"
#include "views/Dialogue/Dialogue.hpp"
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

class VillageRight {
public:
	Player player;
	sf::RenderWindow window;
	std::vector<sf::RectangleShape> collision;
	VillageRight(Player player);
	bool canGo(Player player, int x);
	void initCollision();
};


