#ifndef STARTGAME_HPP
#define STARTGAME_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
#include "views/Player/player.hpp"
#include "views/Dialogue/Dialogue.hpp"
#include "views/Maps/Village/Village.hpp"
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

class StartGame {
	public:
	sf::RenderWindow window;
	StartGame(Player player);

	bool canGo(int i,sf::Sprite player);

};


#endif //STARTGAME_HPP