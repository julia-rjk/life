#include "StartGame.hpp"

StartGame::StartGame(Player player){

	sf::RenderWindow window;
	window.create(sf::VideoMode(900, 600),"Life");
	// sf::Vector2u windowSize = window.getSize();

	int width = sf::VideoMode::getDesktopMode().width;
	int height = sf::VideoMode::getDesktopMode().height;
	window.setPosition(sf::Vector2i(width/2, height/2));

	sf::Clock clock;

	Inventory inventory;

	inventory.setImagePlayer("content/Character/down.png");
	player.setPosition(56,224);

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/Map1.png");
	shape.setTexture(&shapeTexture);


	//Charge l'ecriture de début
	sf::Font font;
	font.loadFromFile("font/pixel.TTF");
	sf::Music soundWalk;
	if (!soundWalk.openFromFile("content/sound/walk_stone.ogg"))
		std::cout<<"Couldn't load music.";

	//Création Label
	sf::Text text("Press E to interact", font, 30);

	//Dialogue open when player press E, we initiate texts.
	Dialogue dialoguePanel;
	std::vector<std::string> dialogue;
	dialogue.push_back("Welcome to this new world " + player.getName() + " !");
	dialogue.push_back("You are going to have\n a wonderful adventure.");
	dialogue.push_back("Villagers are waiting for you !");
	dialogue.push_back("They haven't see in a long time...");
	dialogue.push_back("Follow the path");

	sf::Text textInventory;
	textInventory.setString("Inventory");
	textInventory.setFont(font);
	textInventory.setCharacterSize(30);
	textInventory.setPosition(-100,-100);

	std::size_t nbDialogue = 0;


	while(window.isOpen()){
		sf::Event event;

		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
				if(dialoguePanel.getCadreAffiche()){
					if(clock.getElapsedTime().asSeconds() > 0.25f){
					if(nbDialogue < dialogue.size()){
						nbDialogue =nbDialogue+1;
					}else{
						nbDialogue =0;
					}
					clock.restart();
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
				if(player.getClockAnimation().getElapsedTime().asSeconds() > 0.25){
					player.getClockAnimation().restart();
				}
				if(canGo(0,player.getPlayer())){
					player.moveLeft();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
					if(canGo(1,player.getPlayer())){
					player.moveRight();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
				if(canGo(2,player.getPlayer())){
					player.moveUp();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
				if(canGo(3,player.getPlayer())){
					player.moveDown();
					if(clock.getElapsedTime().asSeconds() > 0.5f){
						soundWalk.play();
						clock.restart();
					}
				}
			}

		}

		if(player.getPlayer().getPosition().x > 148 &&  player.getPlayer().getPosition().x < 208 &&  player.getPlayer().getPosition().y >184 &&  player.getPlayer().getPosition().y < 240){
			text.setPosition(150,500);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)){
					if(clock.getElapsedTime().asSeconds() > 0.2f){
					dialoguePanel.afficherCadre();
					clock.restart();
					}
			}
		}else{
			text.setPosition(-3000,-3000);
		}

		if(player.getPlayer().getPosition().y > 600){
			window.close();
			Village village(player);
		}


		if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)){
			if(clock.getElapsedTime().asSeconds() > 0.2f){
				inventory.setCoin(player.getGold());
				inventory.showInventory();
				textInventory.setPosition(inventory.getCadre().getPosition().x +350, inventory.getCadre().getPosition().y+20);
				clock.restart();
			}
		}

		dialoguePanel.affichePhrase(dialogue[nbDialogue]);


		// std::cout<< " x : "<< player.getPlayer().getPosition().x <<endl;
		// std::cout<< " y : "<< player.getPlayer().getPosition().y <<endl;

		window.clear(sf::Color::Black);
	//
		window.draw(shape);
		window.draw(text);
		window.draw(dialoguePanel.getCadre());
		window.draw(dialoguePanel.getText());
		window.draw(player.getPlayer());

		window.draw(inventory.getCadre());
		window.draw(textInventory);
		for(std::size_t i=0; i<inventory.getAllSprites().size(); i++){
			window.draw(inventory.getSprite(i));
		}
		for(std::size_t i=0; i<inventory.getAllText().size(); i++){
			window.draw(inventory.getText(i));
		}
	//
		window.display();

	}

}

bool StartGame::canGo(int i, sf::Sprite player){

	switch(i){
		//Left
		case 0:
			if(player.getPosition().x-5 > 48 && player.getPosition().y <244)
				return true;
			else if(player.getPosition().x >= 447 && player.getPosition().y >= 244){
				return true;
			}

			break;
		//right
		case 1:
			if(player.getPosition().x-5 < 512)
				return true;
			break;
		//Up
		case 2:
			if(player.getPosition().y-4 > 196)
				return true;
			break;
		//Down
		case 3:
			if(player.getPosition().y+4 < 244 && player.getPosition().x < 432)
				return true;
			else if(player.getPosition().x >=432 && player.getPosition().y+4 > 199)
				return true;
			break;
		default:
			return false;
			break;
	}
	return false;
}