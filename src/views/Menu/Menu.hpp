#ifndef Menu_HPP
#define Menu_HPP

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "views/StartGame/StartGame.hpp"
#include "views/Menu/MenuName.hpp"
#include <chrono>
#include <thread>
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

//Initialise la fenêtre du menu
int createMenu();

#endif //Menu_HPP

