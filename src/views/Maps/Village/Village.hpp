#pragma once

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <chrono>
#include <thread>
#include "views/Player/player.hpp"
#include "views/Dialogue/Dialogue.hpp"
#include "views/StartGame/StartGame.hpp"
#include "VillageRight.hpp"
using namespace std;
using namespace std::this_thread; // sleep_for, sleep_until
using namespace std::chrono;

class Village {
public:
	Player player;
	sf::RenderWindow window;
	std::vector<sf::RectangleShape> collision;
	Village(Player player);
	bool canGo(Player player, int x);
	void initCollision();
	void animateNpc(int x, int y);

	private:
		sf::Clock clock, clockAnimation;
		Inventory inventory;
		sf::IntRect rectSourceSprite;
		sf::Texture textureExclamation;
		sf::Sprite spriteAnimation;
		sf::Sprite spriteNpc;
		sf::Texture textureNpc;

		sf::Music soundWalk, soundExpression;
};


