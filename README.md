# Initial project

I took the git project : https://github.com/andrew-r-king/sfml-vscode-boilerplate to make SFML work on my PC.
Sprites were taken on opengameart.org, I will link/credit authors.

# Resume
Life is simulation of life. You arrive in a town where you are about to start a new life..

# Building
1. Install VSCode  (https://code.visualstudio.com/)
2. Run `sudo apt install -y build-essential`
3. Run `sudo apt install -y libsfml-dev`
4. Open the project in VSCode, and press F5

# Future updates

* Customize character
* More maps
* Implementation of history/game play
