#include "MenuName.hpp"

MenuName::MenuName(){

	sf::RenderWindow window;
	window.create(sf::VideoMode(900, 600),"Life");

	int width = sf::VideoMode::getDesktopMode().width;
	int height = sf::VideoMode::getDesktopMode().height;
	window.setPosition(sf::Vector2i(width/2, height/2));
	sf::Clock clock;

	Player player;
	player.setName("John");

	//Variable qui permet de gérer pour qu'une seule fenêtre soit ouverte
	// bool somethingIsOpen = false;

	//Charger une image de fond
	sf::RectangleShape shape(sf::Vector2f(900, 600));
	shape.setFillColor(sf::Color::White);
	sf::Texture shapeTexture;
	shapeTexture.loadFromFile("content/background/Ocean.png");
	shape.setTexture(&shapeTexture);

	//Charge l'ecriture de début
	sf::Font font;
	font.loadFromFile("font/pixel.TTF");
	sf::Music soundWalk;
	if (!soundWalk.openFromFile("content/sound/walk_stone.ogg"))
		std::cout<<"Couldn't load music.";

	//Création Label

	sf::String playerInput;
	sf::Text playerText(" ", font, 30);
	playerText.setPosition(290,235);

	sf::Text textName("Your name : ", font, 50);
	textName.setPosition(200,80);

	// Input field
	sf::Texture texture;
	if(!texture.loadFromFile("content/UI/inputField.png")){
		std::cout << "Could not load input field png"<<endl;
	}
	sf::Sprite sprite(texture);
	sprite.setPosition(270,220);
	sprite.setScale(1.5,1.5);


	while(window.isOpen()){
		sf::Event event;

		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				std::cout<<"Merci d'avoir joué ! :) \n";
				window.close();
			}
			if (event.type == sf::Event::TextEntered)
			{

				if(playerInput.getSize() < 20){
					playerInput +=event.text.unicode;
					playerText.setString(playerInput);
				}

				// We delete last char with backspace
				if(event.text.unicode == 8 && playerInput.getSize() > 0){
					playerInput = playerInput.substring(0, playerInput.getSize()-2);
					playerText.setString(playerInput);
				}
			}

			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
				window.close();
				player.setName(playerInput);
				StartGame start(player);
			}


		}

		window.clear(sf::Color::Black);
	//
		// Background
		window.draw(shape);
		// Others
		window.draw(sprite);
		window.draw(playerText);
		window.draw(textName);

	//
		window.display();

	}

}
