#include "Dialogue.hpp"

Dialogue::Dialogue(){
	// int nbPhrase = 1;
	font.loadFromFile("font/pixel.TTF");
	cadre.setSize(sf::Vector2f(800, 170));
	cadre.setOutlineColor(sf::Color::Black);
	cadre.setOutlineThickness(5);
	cadre.setPosition(-8000, -4000);
	texture.loadFromFile("content/background/Ocean.png");
	cadre.setTexture(&texture);
	std::size_t i=100;
	phrases.reserve(i);
	cadreAffiche = false;

}

sf::RectangleShape Dialogue::getCadre(){
	return cadre;
}

void Dialogue::addPhrase(string phrase){
		text.setString(phrase);
		text.setFont(font);
		text.setFillColor(sf::Color::Black);
		text.setCharacterSize(24);
		text.setPosition(-6000,-4050);
		phrases.push_back(text);
}

void Dialogue::removeCadre(){
	if(cadreAffiche){
		cadre.setPosition(-8000, -4000);
		cadreAffiche = false;
	}
}

void Dialogue::afficherCadre(){
	if(!cadreAffiche){
	cadre.setPosition(20, 400);
	cadreAffiche = true;
	}else{

		cadre.setPosition(-8000, -4000);
		cadreAffiche = false;
	}
}

bool Dialogue::getCadreAffiche(){
	return cadreAffiche;
}

void Dialogue::affichePhrase(string phrase){
	//Création Label
	if(cadreAffiche){
		text.setString(phrase);
		text.setFont(font);
		text.setFillColor(sf::Color::Black);
		text.setCharacterSize(24);
		text.setPosition(60,450);
	}else{
		text.setPosition(-6000,-4050);
	}
}

sf::Text Dialogue::getText()
{
	return text;
}

std::vector<sf::Text> Dialogue::getDialogue()
{
	return phrases;
}

void Dialogue::erase(int i){
	phrases.erase(phrases.begin()+i);
}
