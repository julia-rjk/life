#include "player.hpp"

Player::Player(){
	textureSprite.loadFromFile("content/Character/down.png");
	player.setTexture(textureSprite);
	player.setScale(1.5,1.5);
	gold = 0;
	start = 0;
	right = false;
}

sf::Sprite Player::getPlayer(){
	return player;
}

// Can be positive or negative
void Player::setGold(int addGold){
	gold +=addGold;
}

int Player::getGold(){
	return gold;
}

void Player::setName(string newName){
	name = newName;
}

string Player::getName(){
	return name;
}

void Player::moveRight(){
	if(this->getRight()){
		textureSprite.loadFromFile("content/Character/right.png");
		this->setRight(false);
	}else{
		textureSprite.loadFromFile("content/Character/right2.png");
		this->setRight(true);
	}
	player.setTexture(textureSprite);
	player.move(4,0);
}

void Player::setRight(bool cond){
	right = cond;
}

bool Player::getRight(){
	return right;
}

void Player::moveLeft(){
	if(clockAnimation.getElapsedTime().asSeconds() < 0.25){
		textureSprite.loadFromFile("content/Character/left.png");
	}else{
		textureSprite.loadFromFile("content/Character/left2.png");
	}
	player.setTexture(textureSprite);
	player.move(-4,0);

}

int Player::getX(){
	return player.getPosition().x ;
}
int Player::getY(){
	return player.getPosition().y ;
}

void Player::moveUp(){

	if(clockAnimation.getElapsedTime().asSeconds() < 0.25){
		textureSprite.loadFromFile("content/Character/up.png");
	}else{
		textureSprite.loadFromFile("content/Character/up2.png");
	}
	player.setTexture(textureSprite);
	player.move(0,-4);
}
void Player::moveDown(){
	if(clockAnimation.getElapsedTime().asSeconds() < 0.25){
		textureSprite.loadFromFile("content/Character/down.png");
	}else{
		textureSprite.loadFromFile("content/Character/down2.png");
		clockAnimation.restart();
	}
	player.setTexture(textureSprite);
	player.move(0,4);
}

sf::Clock Player::getClockAnimation(){
	return clockAnimation;
}

void Player::setPosition(int x,int y){
	player.setPosition(x,y);
}

void Player::setTexture(std::string path){
	if (textureSprite.loadFromFile(path)){
		player.setTexture(textureSprite);
	}
}

Inventory Player::getInventory(){
	return inventory;
}