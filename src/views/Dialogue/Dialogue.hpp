#ifndef DIALOGUE_HPP
#define DIALOGUE_HPP
#include "views/Menu/Menu.hpp"

class Dialogue{
	public:
	bool cadreAffiche = false;
	sf::RectangleShape cadre;
	sf::Texture texture;
	sf::Text text;
	std::vector<sf::Text> phrases;
	sf::Font font;
	Dialogue();
	sf::RectangleShape getCadre();
	void addPhrase(string phrase);
	void affichePhrase(string phrase);
	void removeCadre();
	void afficherCadre();
	sf::Text getText();
	std::vector<sf::Text> getDialogue();
	bool getCadreAffiche();
	void erase(int i);

};

#endif //DIALOGUE_HPP